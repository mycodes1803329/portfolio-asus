jQuery(document).ready(function() {
  jQuery(".card.content-container > section.card-body:not('#aboutMe')").hide();

  showContent('aboutMe');

  jQuery("div.list-group a").click(function(e) {
    e.preventDefault();
    let target = jQuery(this).data("target");
    showContent(target);
  });

  function showContent(contentID) {
    jQuery(".card.content-container > section.card-body").hide();
    jQuery("#" + contentID).show();
  }
});


// jQuery(document).ready(function(){
//   jQuery(".tabcontent").hide();

//   showContent('webDeveloper');

//   jQuery("tablinks").click(function(e){
//     e.preventDefault();
//     let target = jQuery(this).data("target");
//     showContent(target);
//   });

//   function showContent(contentID){
//     jQuery(".tabcontent").hide();
//     jQuery("#" contentID).show();
//   }
// });
